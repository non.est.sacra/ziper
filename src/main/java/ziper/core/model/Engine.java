package ziper.core.model;

import com.esotericsoftware.yamlbeans.YamlReader;
import ziper.core.model.handler.RestHandler;
import zoomba.lang.core.collections.ZArray;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.util.Collections;
import java.util.Map;

/**
 */
public class Engine {

    final String modelFile;

    Map<String, Object> config;

    final File myLocation;

    public Map<String, String> queries() {
        return (Map<String, String>) config.getOrDefault("queries", Collections.emptyMap());
    }

    public Map<String, String> tables() {
        return (Map<String, String>) config.getOrDefault("tables", Collections.emptyMap());
    }

    public Engine(String fileLocation) {
        try {
            YamlReader yamlReader = new YamlReader(new FileReader(fileLocation));
            config = (Map) yamlReader.read();
            modelFile = fileLocation;
            myLocation = new File(modelFile);

        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    private ZoomBAModel zoomBAModel(String zmbString) {
        try {
            if (zmbString.startsWith("_/")) {
                String filePath = myLocation.getParent() + "/" + zmbString.substring(2);
                if ( !filePath.endsWith( ".zmb") ){
                    filePath += ".zmb" ;
                }
                zmbString = new String(Files.readAllBytes(new File(filePath).toPath()));
            }
            return new ZoomBAModel(zmbString, tables());
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    public Object execute(String query, Object[] args) {
        String zmbString = queries().getOrDefault(query, "");
        if (zmbString.isEmpty()) {
            return null;
        }
        ZoomBAModel zoomBAModel = zoomBAModel(zmbString);
        return zoomBAModel.execute(RestHandler.dsContext( tables()), args);
    }

    public Object execute(String query) {
        return execute(query, ZArray.EMPTY_ARRAY);
    }
}
