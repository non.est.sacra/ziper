package ziper.core.model;

import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;

import java.util.Collections;
import java.util.Map;

/**
 */
public class ZoomBAModel {

    public final String scriptText;

    public final ZScript script;

    public final Map<String,String> tables;

    public ZoomBAModel(String scriptText, Map<String,String> tables){
        try {
            script = new ZScript(scriptText);
            this.scriptText = scriptText;
            this.tables = tables ;
        }catch (Throwable t){
            throw new RuntimeException(t);
        }
    }

    public Object execute(Map<String,Object> dsContext, Object[] args){
        ZContext.FunctionContext functionContext = new ZContext.FunctionContext(
                new ZContext.EmptyContext(), new ZContext.ArgContext(args));
        for (Map.Entry<String,Object> p : dsContext.entrySet() ){
            functionContext.set(p.getKey(),p.getValue());
        }
        script.setExternalContext(functionContext);
        Function.MonadicContainer mc = script.execute( args );
        return mc.value();
    }

}
